import React from "react";
import logo from "./logo.svg";
import "./App.css";
import BarChartComponent from "./Components/BarChartComponent";

function App() {
  return (
    <div className="App">
      <BarChartComponent />
    </div>
  );
}

export default App;
